package com.factweavers.AmlGraph.Datastore;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Neo4jConnector {

    private  static Driver driver =null;


    private static Neo4jConnector ourInstance = new Neo4jConnector();

    public static Driver getInstance() {

        if(driver==null)
        {
            ourInstance = new Neo4jConnector();
            System.out.println("Empty");

        }
        return driver;
    }

    private Neo4jConnector() {

      //  driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic("neo4j", "1234"));
        driver = GraphDatabase.driver("bolt://insportshq.factweavers.com:7687", AuthTokens.basic("neo4j", "factweavers!@#"));
        if(driver!=null)
        {
            System.out.println("Driver Connected");
        }

    }
}
