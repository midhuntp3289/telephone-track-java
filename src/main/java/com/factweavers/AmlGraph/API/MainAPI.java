package com.factweavers.AmlGraph.API;

import com.factweavers.AmlGraph.Datastore.Neo4jConnector;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class MainAPI {

    Driver driver =null;
    Session session = null;
       public MainAPI() {

           driver = Neo4jConnector.getInstance();
           if(driver!=null) {
               session = driver.session();
           }
           else {
               System.out.println("Driver not connected ,Existing");
               System.exit(0);
           }

    }




    public void getPath(RoutingContext routingContext) {

           try{

               String from = "102-097-5329";
               from = routingContext.request().getParam("from");
               String to = "140-327-5418";
               to = routingContext.request().getParam("to");
              // String hopeCount = routingContext.request().getParam("hopes");
             String hopeCount = "5";
              // String limit = routingContext.request().getParam("limit");
             String limit = "10";
               Gson gson = new Gson();
               String query = " MATCH p=(u:Person{contactNo:'"+from+"'})-[r:called*1.."+hopeCount+"]-(v:Person{contactNo:'"+to+"'}) RETURN p  LIMIT "+limit;

              System.out.println(query);

             //  routingContext.vertx().e
               StatementResult result = session.run( query );

               io.vertx.core.json.JsonObject response = new io.vertx.core.json.JsonObject();
               io.vertx.core.json.JsonArray nodes = new io.vertx.core.json.JsonArray();
               io.vertx.core.json.JsonArray relationships = new io.vertx.core.json.JsonArray();
               io.vertx.core.json.JsonObject temp =new io.vertx.core.json.JsonObject();

               io.vertx.core.json.JsonArray Pnodes = new io.vertx.core.json.JsonArray();
               io.vertx.core.json.JsonArray Prelationships = new io.vertx.core.json.JsonArray();

               io.vertx.core.json.JsonArray data = new io.vertx.core.json.JsonArray();


               while ( result.hasNext() ) {
                   Record record = result.next();

                   String jsonInString = gson.toJson(record.asMap());
                  io.vertx.core.json.JsonObject object = new io.vertx.core.json.JsonObject(jsonInString);

                  Prelationships.add(object);
                /*  io.vertx.core.json.JsonObject path =object.getJsonObject("p");

                   nodes=path.getJsonArray("nodes");

                   nodes.forEach(tmp->{
                      io.vertx.core.json.JsonObject usr = (io.vertx.core.json.JsonObject)tmp;

                       String label =usr.getJsonArray("labels").getString(0);
                       usr.put("labels",label);
                       String contact=usr.getJsonObject("properties").getJsonObject("contact").getString("val");
                       usr.getJsonObject("properties").put("contact",contact);

                    Pnodes.add(tmp);
                   });


                   relationships=path.getJsonArray("relationships");

                  relationships.forEach(tmp->{
                       io.vertx.core.json.JsonObject rel = (io.vertx.core.json.JsonObject)tmp;

                     String label =rel.getString("type");
                       rel.put("caption",label);

                       int source =rel.getInteger("start");
                       rel.put("startNode",source);
                       rel.remove("start");

                       int target =rel.getInteger("end");
                       rel.put("endNode",target);
                      rel.remove("end");

                      Prelationships.add(rel);


                   });


                  // System.out.println(jsonInString);*/
               }

               response.put("relations",Prelationships);
           /*    temp.put("nodes",Pnodes);
               temp.put("relationships",Prelationships);


               io.vertx.core.json.JsonObject graph =new io.vertx.core.json.JsonObject();

               graph.put("graph",temp);

               data.add(graph);


               io.vertx.core.json.JsonObject parent =new io.vertx.core.json.JsonObject();


               parent.put("results",new io.vertx.core.json.JsonArray().add(
                       new io.vertx.core.json.JsonObject().put("data",data)
               ) );


               System.out.println(parent.encodePrettily());*/



               System.out.println("Query excepted and result collected");

               routingContext.response()
                       .putHeader("content-type", "application/json; charset=utf-8")
                       .end(response.encodePrettily());

           }catch(Exception ex)
           {

               System.out.println("Exception in get path"+ex.toString());
           }


    }


    public void getPathAllinOne(RoutingContext routingContext) {

        try{

            String from = "102-097-5329";
            from = routingContext.request().getParam("from");
            String to = "136-161-6863";
            to = routingContext.request().getParam("to");
            // String hopeCount = routingContext.request().getParam("hopes");
            String hopeCount = "5";
            // String limit = routingContext.request().getParam("limit");
            String limit = "10";
            Gson gson = new Gson();
           String query = "MATCH p=(u:Person{contactNo:'"+from+"'})-[r:called*1.."+hopeCount+"]-(v:Person{contactNo:'"+to+"'}) RETURN p  LIMIT "+limit;
            // String query = " MATCH p=(u:user{contact:'"+from+"'})-[r:called*1.."+hopeCount+"]-(v:user{contact:'"+to+"'}) RETURN p  LIMIT "+limit;

            System.out.println(query);

            //  routingContext.vertx().e
            StatementResult result = session.run( query );

            io.vertx.core.json.JsonObject response = new io.vertx.core.json.JsonObject();
            io.vertx.core.json.JsonArray nodes = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonArray relationships = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonObject temp =new io.vertx.core.json.JsonObject();

            io.vertx.core.json.JsonArray Pnodes = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonArray Prelationships = new io.vertx.core.json.JsonArray();

            io.vertx.core.json.JsonArray data = new io.vertx.core.json.JsonArray();


            while ( result.hasNext() ) {
                Record record = result.next();

                String jsonInString = gson.toJson(record.asMap());
                io.vertx.core.json.JsonObject object = new io.vertx.core.json.JsonObject(jsonInString);

              //  Prelationships.add(object);
                  io.vertx.core.json.JsonObject path =object.getJsonObject("p");

                   nodes=path.getJsonArray("nodes");

                   nodes.forEach(tmp->{
                      io.vertx.core.json.JsonObject usr = (io.vertx.core.json.JsonObject)tmp;

                       String label =usr.getJsonArray("labels").getString(0);
                       usr.put("labels",label);
                       String contact=usr.getJsonObject("properties").getJsonObject("contactNo").getString("val");
                       usr.getJsonObject("properties").put("contact",contact);

                    Pnodes.add(tmp);
                   });


                   relationships=path.getJsonArray("relationships");

                  relationships.forEach(tmp->{
                       io.vertx.core.json.JsonObject rel = (io.vertx.core.json.JsonObject)tmp;

                     String label =rel.getString("type");
                       rel.put("caption",label);

                       int source =rel.getInteger("start");
                       rel.put("startNode",source);
                       rel.remove("start");

                       int target =rel.getInteger("end");
                       rel.put("endNode",target);
                      rel.remove("end");

                      Prelationships.add(rel);


                   });


                  // System.out.println(jsonInString);*/
            }

           // response.put("relations",Prelationships);
               temp.put("nodes",Pnodes);
               temp.put("relationships",Prelationships);


               io.vertx.core.json.JsonObject graph =new io.vertx.core.json.JsonObject();

               graph.put("graph",temp);

               data.add(graph);


               io.vertx.core.json.JsonObject parent =new io.vertx.core.json.JsonObject();


               parent.put("results",new io.vertx.core.json.JsonArray().add(
                       new io.vertx.core.json.JsonObject().put("data",data)
               ) );


               System.out.println(parent.encodePrettily());



            System.out.println("Query excepted and result collected");

            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(parent.encodePrettily());

        }catch(Exception ex)
        {

            System.out.println("Exception in get path"+ex.toString());
        }


    }



    public void getPathSequential(RoutingContext routingContext) {

        try{

            String from = "102-097-5329";
            from = routingContext.request().getParam("from");
            String to = "140-327-5418";
            to = routingContext.request().getParam("to");
            // String hopeCount = routingContext.request().getParam("hopes");
            String hopeCount = "5";
            // String limit = routingContext.request().getParam("limit");
            String limit = "10";
            Gson gson = new Gson();
            String query = " MATCH p=(u:Person{contactNo:'"+from+"'})-[r:called*1.."+hopeCount+"]-(v:Person{contactNo:'"+to+"'}) RETURN relationships(p)  LIMIT "+limit;

            System.out.println(query);

            //  routingContext.vertx().e
            StatementResult result = session.run( query );

            io.vertx.core.json.JsonObject response = new io.vertx.core.json.JsonObject();
            io.vertx.core.json.JsonArray nodes = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonArray relationships = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonObject temp =new io.vertx.core.json.JsonObject();

            io.vertx.core.json.JsonArray Pnodes = new io.vertx.core.json.JsonArray();
            io.vertx.core.json.JsonArray Prelationships = new io.vertx.core.json.JsonArray();

            io.vertx.core.json.JsonArray data = new io.vertx.core.json.JsonArray();


            while ( result.hasNext() ) {
                Record record = result.next();

                String jsonInString = gson.toJson(record.asMap());
                io.vertx.core.json.JsonObject object = new io.vertx.core.json.JsonObject(jsonInString);

                Pnodes =object.getJsonArray("relationships(p)");



                Pnodes.forEach(relation->{
                    try {


                        io.vertx.core.json.JsonObject IndividualRel = (io.vertx.core.json.JsonObject) relation;
                        String time = IndividualRel.getJsonObject("properties").getJsonObject("time").getString("val");
                      //  System.out.println(time);
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
                        Date dt = df.parse(time);
                        System.out.println(dt);
                    }
                    catch (Exception ex)
                    {
                        System.out.println("Exception while date parse"+ex.toString());
                    }
                });



                Prelationships.add(object);
                /*  io.vertx.core.json.JsonObject path =object.getJsonObject("p");

                   nodes=path.getJsonArray("nodes");

                   nodes.forEach(tmp->{
                      io.vertx.core.json.JsonObject usr = (io.vertx.core.json.JsonObject)tmp;

                       String label =usr.getJsonArray("labels").getString(0);
                       usr.put("labels",label);
                       String contact=usr.getJsonObject("properties").getJsonObject("contact").getString("val");
                       usr.getJsonObject("properties").put("contact",contact);

                    Pnodes.add(tmp);
                   });


                   relationships=path.getJsonArray("relationships");

                  relationships.forEach(tmp->{
                       io.vertx.core.json.JsonObject rel = (io.vertx.core.json.JsonObject)tmp;

                     String label =rel.getString("type");
                       rel.put("caption",label);

                       int source =rel.getInteger("start");
                       rel.put("startNode",source);
                       rel.remove("start");

                       int target =rel.getInteger("end");
                       rel.put("endNode",target);
                      rel.remove("end");

                      Prelationships.add(rel);


                   });


                  // System.out.println(jsonInString);*/
            }

            response.put("relations",Prelationships);
           /*    temp.put("nodes",Pnodes);
               temp.put("relationships",Prelationships);


               io.vertx.core.json.JsonObject graph =new io.vertx.core.json.JsonObject();

               graph.put("graph",temp);

               data.add(graph);


               io.vertx.core.json.JsonObject parent =new io.vertx.core.json.JsonObject();


               parent.put("results",new io.vertx.core.json.JsonArray().add(
                       new io.vertx.core.json.JsonObject().put("data",data)
               ) );


               System.out.println(parent.encodePrettily());*/



            System.out.println("Query excepted and result collected");

            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(response.encodePrettily());

        }catch(Exception ex)
        {

            System.out.println("Exception in get path"+ex.toString());
        }


    }
}
