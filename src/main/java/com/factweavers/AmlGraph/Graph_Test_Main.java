package com.factweavers.AmlGraph;

import com.factweavers.AmlGraph.API.MainAPI;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class Graph_Test_Main extends AbstractVerticle {
    MainAPI mainAPI = new MainAPI();

    @Override
    public void start(Future<Void> fut) throws Exception {
        startWebApp((http) -> completeStartup(http, fut));
    }

    private void completeStartup(AsyncResult<HttpServer> http, Future<Void> fut) {
        if (http.succeeded()) {
            fut.complete();
        } else {
            fut.fail(http.cause());
        }
    }

    private void startWebApp(Handler<AsyncResult<HttpServer>> next) {

       try{

           Router router =Router.router(vertx);
           router.route().handler(BodyHandler.create());
           router.route().handler(CorsHandler.create("*")
                   .allowedMethod(HttpMethod.GET)
                   .allowedMethod(HttpMethod.POST)
                   .allowedMethod(HttpMethod.OPTIONS)
                   .allowedHeader("X-PINGARUNER")
                   .allowedHeader("Authorization")
                   .allowedHeader("Content-Type")

           );

           router.get("/getPath").handler(mainAPI::getPathAllinOne);
           router.get("/getPathSequential").handler(mainAPI::getPathSequential);

           vertx.createHttpServer()
                   .requestHandler(router::accept)
                   .listen(9010, "0.0.0.0");

       }catch(Exception ex)
       {
           System.out.println("Exception while connecting to graph  "+ex.toString());

       }

    }
}
